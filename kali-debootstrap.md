To install Kali Linux by Offensive Security, in  a debootstrap environment, you need to setup some files first.

If you try to use deboostrap with kali-rolling you'll get an error.

<code><b># nano /usr/share/debootstrap/scripts/kali-rolling</b></code>

and enter this text:

<code>mirror_style release
download_style apt
finddebs_style from-indices
keyring /usr/share/keyrings/kali-archive-keyring.gpg

<code>if [ -d /usr/share/debootstrap/scripts ]; then
 . /usr/share/debootstrap/scripts/debian-common
elif [ -e "$DEBOOTSTRAP_DIR/scripts/debian-common" ]; then
 . "$DEBOOTSTRAP_DIR/scripts/debian-common"
else
 . /debootstrap/debian-common
fi

</code>

with that, you can use debootstrap, but you'll still need the keyring, get it like this:

<code><b>wget https://http.kali.org/kali/pool/main/k/kali-archive-keyring/kali-archive-keyring_2018.1_all.deb</b></code>

then install it, through dpkg or gdebi, it doesn't matter.

<code><b>sudo dpkg -i kali-archive-keyring_2018.1_all.deb</b></code>

Now you can proceed to install kali linux with this command:

<code><b># debootstrap kali-rolling kali-rolling http://http.kali.org/kali</b></code>

But if you try to use apt there, it will fail, since the sources.list is incorrect, change it like this:
(I'm asumming you are already in the chroot)

<code><b>nano /etc/apt/sources.list</b></code>

and enter the following:

<code>deb http://http.kali.org/kali kali-rolling main contrib non-free</code>

and well, you also need to add the keyring to kali, you can execute the commands again, but in the chroot jail.

Finally, you can use it! (notice, it comes with the bare minimum, so you need to install the programs).